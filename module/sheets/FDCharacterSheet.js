import * as Dice from "../dice.js";

export default class FDCharacterSheet extends ActorSheet {
    static get defaultOptions() {
        return mergeObject(super.defaultOptions, {
            classes: ["fd", "sheet", "character"]
        });
    }

    get template() {
        return `systems/fkng_doors/templates/sheets/${this.actor.data.type}-sheet.hbs`;
    }

    getData() {
        const data = super.getData();
        data.config = CONFIG.fd;


        data.weapons = data.items.filter(function (item) {
            return item.type == "weapon" && item.data.equipped
        })
        data.spells = data.items.filter(function (item) {
            return item.type == "spell"
        }).sort(data.config.itemSortingFunction);

        data.combatSkills = data.items.filter(function (item) {
            //return (item.type == "skill" && !item.data.isPassive && item.data.isCombatSkill) -> Combat Skills are not included
            return (item.type == "skill" && item.data.isCombatSkill) // Combat Skills are included
        }).sort(data.config.itemSortingFunction);

        data.standardSkills = data.items.filter(function (item) {
            return (item.type == "skill" && !item.data.isPassive && !item.data.isCombatSkill)
        }).sort(data.config.itemSortingFunction);

        data.passiveSkills = data.items.filter(function (item) {
            return (item.type == "skill" && item.data.isPassive && !item.data.isCombatSkill) // Combat Skills are excluded
        }).sort(data.config.itemSortingFunction);



        //data.macros = this.actor.getOwnedMacros();

        let wornArmors = data.items.filter(function (item) {
            return item.type == "armor" && item.data.equipped
        })
        data.armors = {
            head: wornArmors.filter(function (item) {
                return item.data.slot == 'head'
            }),
            torso: wornArmors.filter(function (item) {
                return item.data.slot == 'torso'
            }),
            arm: wornArmors.filter(function (item) {
                return item.data.slot == 'arm'
            }),
            hand: wornArmors.filter(function (item) {
                return item.data.slot == 'hand'
            }),
            leg: wornArmors.filter(function (item) {
                return item.data.slot == 'leg'
            }),
            foot: wornArmors.filter(function (item) {
                return item.data.slot == 'foot'
            }),
            shield: wornArmors.filter(function (item) {
                return item.data.slot == 'shield'
            })
        }

        data.backpackItems = data.items.filter(function (item) {
            return item.data.storage == "backpack" && (item.type == "potion" || item.type == "gourd" || item.type == "basic" || ((item.type == "armor" || item.type == "weapon") && !item.data.equipped))
        }).sort(data.config.itemSortingFunction);

        data.beltItems = data.items.filter(function (item) {
            return item.data.storage == "belt" && (item.type == "potion" || item.type == "gourd" || item.type == "basic" || ((item.type == "armor" || item.type == "weapon") && !item.data.equipped))
        }).sort(data.config.itemSortingFunction);

        data.pouchItems = data.items.filter(function (item) {
            return item.data.storage == "pouch" && (item.type == "potion" || item.type == "gourd" || item.type == "basic" || ((item.type == "armor" || item.type == "weapon") && !item.data.equipped))
        }).sort(data.config.itemSortingFunction);

        data.unstoredItems = data.items.filter(function (item) {
            return (item.data.storage != "backpack" && item.data.storage != "belt" && item.data.storage != "pouch") && (item.type == "potion" || item.type == "gourd" || item.type == "basic" || ((item.type == "armor" || item.type == "weapon") && !item.data.equipped))
        }).sort(data.config.itemSortingFunction);

        data.uneqquipedItems = {
            ...data.backpackItems,
            ...data.beltItems,
            ...data.pouchItems,
            ...data.unstoredItems
        }

        data.skills = {
            ...data.combatSkills,
            ...data.standardSkills,
            ...data.passiveSkills
        }

        data.attacks = {
            ...data.spells,
            ...data.skills
        }

        data.isGM = game.user.isGM;

        data.healthThresholds = {
            threshold1: Math.floor(this.actor.data.data.health.max / 4),
            threshold2: Math.floor(this.actor.data.data.health.max / 8)
        }
        data.maxExperience = CONFIG.fd.experienceThresholds[this.actor.data.data.level];

        return data;
    }

    choiceDelete = {
        name: game.i18n.localize("fd.sheet.delete"),
        icon: '<i class="fas fa-trash"></i>',
        callback: element => {
            this.actor.deleteOwnedItem(element.data("item-id"));
        }
    };

    choiceEdit = {
        name: game.i18n.localize("fd.sheet.edit"),
        icon: '<i class="fas fa-edit"></i>',
        callback: element => {
            const item = this.actor.getOwnedItem(element.data("item-id"));
            item.sheet.render(true);
        }
    }

    choiceEquip = {
        name: game.i18n.localize("fd.sheet.equip"),
        icon: '<i class="fas fa-toggle-on"></i>',
        callback: element => {
            const item = this.actor.getOwnedItem(element.data("item-id"));
            if (item.type == "weapon" || item.type == "armor") {
                let updateData = {
                    data: {
                        equipped: !item.data.data.equipped
                    }
                }
                item.update(updateData);
            }
        }
    }

    choiceReduceDurability = {
        name: game.i18n.localize("fd.sheet.reduceDurability"),
        icon: '<i class="fas fa-bolt"></i>',
        callback: element => {
            const item = this.actor.getOwnedItem(element.data("item-id"));
            if (item.type == "armor") {
                let updateData = {
                    data: {
                        durability: Math.max(0, item.data.data.durability-1)
                    }
                }
                item.update(updateData);
            }
        }
    }

    choiceRollToGM = {
        name: game.i18n.localize("fd.sheet.rollGM"),
        icon: '<i class="fas fa-eye-slash"></i>',
        callback: element => {
            const item = this.actor.getOwnedItem(element.data("item-id"));
            if (item.type == "skill" || item.type == "spell") {
                Dice.rollAptitude(item, this.actor, true);
            }
        }
    }

    playerItemContextMenu = [
        this.choiceEquip,
        this.choiceReduceDurability,
        this.choiceDelete
    ]

    gmItemContextMenu = [
        this.choiceEdit,
        ...this.playerItemContextMenu
    ];

    playerAptitudeContextMenu = [
        this.choiceRollToGM,
        this.choiceDelete
    ];

    gmAptitudeContextMenu = [
        this.choiceEdit,
        ...this.playerAptitudeContextMenu
    ]

    activateListeners(html) {
        //Listeners available to anyone
        html.find(".tab-link").click(this._openPanel.bind(this)); //Switching Open Tab
        html.find(".aptitude-name, .item-name").click(this._onCollapsibleCardClick.bind(this)); //Toggling Card Opened or Closed

        //Listeners available for anyone who can edit the sheet
        if (this.isEditable) {

        }

        //Owner-only listeners
        if (this.actor.owner) {
            html.find(".item-count-input").change(this._onItemCountChange.bind(this)); //Changing Quantity of Item
            html.find(".item-icon").click(this._onItemCardClick.bind(this)); //Roll Item 
            html.find(".aptitude-icon").click(this._onAptitudeIconClick.bind(this)); //Roll Aptitude
            html.find(".stats-roll").click(this._onStatRoll.bind(this)); //Roll Stat
            html.find(".food-icon-container").on("click contextmenu", this._onFoodIconClick.bind(this)); //Update of meal eaten today
            html.find(".gourd-icon-container").on("click contextmenu", this._onGourdIconClick.bind(this)); //Update of meal eaten today
            //Context Menus
            if (!game.user.isGM) {
                new ContextMenu(html, ".item-card, .item-equipped-card", this.playerItemContextMenu); //Item Context Menu
                new ContextMenu(html, ".aptitude-card", this.playerAptitudeContextMenu); //Aptitude Context Menu
            }
        }

        //GM-only listeners
        if (game.user.isGM) {
            //Context Menus
            new ContextMenu(html, ".item-card, .item-equipped-card", this.gmItemContextMenu); //Item Context Menu
            new ContextMenu(html, ".aptitude-card", this.gmAptitudeContextMenu); //Aptitude Context Menu

            html.find(".selector").change(this._updateNativeValues.bind(this)); //Update Native Values according to new Value in Race/Class selectors   
        }

        super.activateListeners(html);
    }

    _onStatRoll(event) {
        event.preventDefault();
        const statName = event.currentTarget.dataset.statType;
        let stat = this.actor.data.data[statName];
        let statImg = this.getData().config.stats[statName].img;
        Dice.rollStat(game.i18n.localize("fd.actors.commons." + statName), statImg, stat, this.actor);
    }

    _onItemCardClick(event) {
        event.preventDefault();
        const cardId = event.currentTarget.dataset.itemId;
        let item = this.actor.getOwnedItem(cardId);
        if (item.type == "weapon" && item.data.data.equipped) {
            Dice.rollWeaponBasicAttack(item, this.actor);
        } else if (item.type == "potion") {
            Dice.rollItem(item);
        } else if (item.type == "armor" && item.data.data.equipped) {
            Dice.getArmorDialog(item, this.getData().config.armorTypes[item.data.data.type]);
        } else {
            Dice.rollItem(item);
        }
    }

    _onFoodIconClick(event) {
        event.preventDefault();
        let currentMealCount = this.actor.data.data.mealToday;
        let newCount;

        if (event.type == "click") {
            newCount = Math.min(currentMealCount + 1, this.actor.data.data.timeToEat);
        } else {
            newCount = Math.max(currentMealCount - 1, 0);
        }

        this.actor.update({
            "data.mealToday": newCount
        });
    }

    _onGourdIconClick(event) {
        event.preventDefault();
        let itemId = event.currentTarget.dataset.itemId;
        let gourd = this.actor.getOwnedItem(itemId);
        let newCount = gourd.data.data.currentCapacity;

        if (event.type == "click") {
            newCount = Math.min(gourd.data.data.currentCapacity + 1, gourd.data.data.maxCapacity);
        } else {
            newCount = Math.max(gourd.data.data.currentCapacity - 1, 0);
        }

        gourd.update({
            "data.currentCapacity": newCount
        });
    }

    _onCollapsibleCardClick(event) {
        event.preventDefault();
        let collapsibleToggler = event.currentTarget.parentNode;
        let collapsible = collapsibleToggler.nextElementSibling;
        collapsibleToggler.classList.toggle("active");
        if (collapsible.style.maxHeight) {
            collapsible.style.maxHeight = null;
        } else {
            collapsible.style.maxHeight = collapsible.scrollHeight + "px";
        }
    }

    _onAptitudeIconClick(event) {
        event.preventDefault();
        const aptitudeId = event.currentTarget.dataset.itemId;
        let aptitude = this.actor.getOwnedItem(aptitudeId);
        Dice.rollAptitude(aptitude, this.actor, false);
    }

    _onItemCountChange(event) {
        event.preventDefault();
        const itemId = event.target.dataset.itemId;
        const value = event.target.value;
        let item = this.actor.getOwnedItem(itemId);

        let updateData = {
            data: {
                count: value
            }
        };
        item.update(updateData);
    }

    _updateNativeValues(evt) {
        evt.preventDefault();
        let datas = this.getData();

        let race;
        let job;
        if (evt.target.className.includes("job-selector")) {
            race = this.actor.data.data.race;
            job = evt.target.value;
        } else if (evt.target.className.includes("race-selector")) {
            race = evt.target.value;
            job = this.actor.data.data.job;
        }

        let jobObject = datas.config.jobs[job];
        let raceObject = datas.config.races[race];
        let natives = {}
        let dataToUpdate = {
            data: {
                strength: {
                    max: 0,
                    native: 0
                },
                charisma: {
                    max: 0,
                    native: 0
                },
                agility: {
                    max: 0,
                    native: 0
                },
                intelligence: {
                    max: 0,
                    native: 0
                },
                health: {
                    native: 0
                },
                mana: {
                    native: 0
                },
                luck: {
                    native: 0
                },
                senses: {
                    native: 0
                }
            }
        };
        for (const [attributeName, attributeValues] of Object.entries(jobObject.attributes)) {
            if ('native' in attributeValues) {
                if (attributeName in natives) {
                    natives[attributeName] += attributeValues.native;
                } else {
                    natives[attributeName] = attributeValues.native;
                }
            }
        }

        for (const [attributeName, attributeValues] of Object.entries(raceObject.attributes)) {
            if ('native' in attributeValues) {
                if (attributeName in natives) {
                    natives[attributeName] += attributeValues.native;
                } else {
                    natives[attributeName] = attributeValues.native;
                }
            }

            if ('max' in attributeValues) {
                dataToUpdate.data[attributeName].max = attributeValues['max'];
            }
        }

        for (let i = 0; i < Object.keys(CONFIG.fd.stats).length; i++) {
            let stat = Object.keys(CONFIG.fd.stats)[i];
            let newValue = 0;
            if (natives.hasOwnProperty(stat)) {
                newValue = natives[stat];
            }

            dataToUpdate.data[stat].native = newValue;
        }
        this.actor.update(dataToUpdate);
    }

    _openPanel(evt) {
        evt.preventDefault();
        let element = evt.currentTarget;

        this._openPanelByClass(element.dataset.panelToOpen);
        this.actor.update({
            data: {
                openedSheetPanel: element.dataset.panelToOpen
            }
        });
        element.className += " active"
    }

    _openPanelByClass(panel) {
        var i, tabContent, tabLinks;
        tabContent = document.getElementsByClassName("tab-content");
        for (i = 0; i < tabContent.length; i++) {
            tabContent[i].style.display = "none";
        }

        tabLinks = document.getElementsByClassName("tab-link");
        for (i = 0; i < tabLinks.length; i++) {
            tabLinks[i].className = tabLinks[i].className.replace(" active", "");
        }

        document.getElementById(panel).style.display = "block";
    }
}