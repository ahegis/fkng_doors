export default class FDActor extends Actor {

    getOwnedMacros() {
        return this.ownedMacros;
    }

    getOwnedMacro(id) {
        if (this.ownedMacros) {
            for (let macro in this.ownedMacros) {
                if (macro.id == id) {
                    return macro;
                }
            }
        }
        return null;
    }

    addOwnedMacro(p_macro) {
        console.log(p_macro);
        if (!this.ownedMacros) {
            this.ownedMacros = [p_macro];
        }
        let alreadyOwned = false;
        for (let macro in this.ownedMacros) {
            if (macro.id == p_macro.id) {
                alreadyOwned = true;
            }
        }

        if (!alreadyOwned) {
            this.ownedMacros.push(p_macro);
        }
    }

    removeOwnedMacro(id) {
        if (this.ownedMacros) {
            for (let macro in this.ownedMacros) {
                if (macro.id == id) {
                    this.ownedMacros.splice(macro, 1);
                }
            }
        }
        return;
    }
}