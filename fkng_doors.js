import {
    fd
} from "./module/config.js";
import FDItemSheet from "./module/sheets/FDItemSheet.js";
import FDCharacterSheet from "./module/sheets/FDCharacterSheet.js";
import FDActor from "./module/entities/FDActor.js"

async function preloadHandlebarsTemplates() {
    const templatePaths = [
        "systems/fkng_doors/templates/partials/card/aptitude-card.hbs",
        "systems/fkng_doors/templates/partials/card/item-card.hbs",
        "systems/fkng_doors/templates/partials/card/item-equipped-card.hbs",
    ];

    return loadTemplates(templatePaths);
}

Hooks.once("init", function () {

    console.log("fkng_doors | Initializing Fuck*ng Doors System");

    CONFIG.fd = fd;
    //CONFIG.Actor.entityClass = FDActor;

    Items.unregisterSheet("core", ItemSheet);
    Items.registerSheet("fkng_doors", FDItemSheet, {
        makeDefault: true
    });
    Actors.unregisterSheet("core", ActorSheet);
    Actors.registerSheet("fkng_doors", FDCharacterSheet, {
        makeDefault: true
    });

    preloadHandlebarsTemplates();

    Handlebars.registerHelper("ifEquals", function (arg1, arg2, options) {
        return (arg1 == arg2) ? options.fn(this) : options.inverse(this);
    });

    Handlebars.registerHelper("times", function (n, content) {
        let result = "";
        for (let i = 0; i < n; ++i) {
            content.data.index = i + 1;
            result += content.fn(i);
        }
        return result;
    });
});