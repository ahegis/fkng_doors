export default class FDItemSheet extends ItemSheet {
    static get defaultOptions() {
        return mergeObject(super.defaultOptions, {
            width: 530,
            height: 340,
            classes: ["fd", "sheet", "weapon"]
        })
    }

    get template() {
        if ((this.item.data.type == "skill") || (this.item.data.type == "spell")) return `systems/fkng_doors/templates/sheets/aptitude-sheet.hbs`;
        if (this.item.data.type == "basic") return `systems/fkng_doors/templates/sheets/basic-item-sheet.hbs`;
        return `systems/fkng_doors/templates/sheets/${this.item.data.type}-sheet.hbs`;
    }

    getData() {
        const data = super.getData();

        data.config = CONFIG.fd;

        return data;
    }

    activateListeners(html) {
        //Listeners available to anyone

        //Listeners available for anyone who can edit the sheet
        if (this.isEditable) {
            html.find(".weaponType-selector").change(this._onWeaponTypeChange.bind(this));
            //html.find(".level-input").change(this._onLevelChange.bind(this));
        }

        //Owner-only listeners
        if (this.item.owner) {}

        super.activateListeners(html);
    }

    _onWeaponTypeChange(event) {
        console.log("onWeaponTypeChange");
        let datas = this.getData();
        let newType = event.target.value;
        let weaponFormula = datas.config.weaponTypes[newType].formula;
        let newBonusDamages = datas.config.weaponTypes[newType].bonusDamages[this.item.data.data.level];
        let dataToUpdate = {
            data: {
                statsFormula: weaponFormula ? weaponFormula : this.item.data.data.statsFormula,
                //bonusDamages: newBonusDamages
            }
        };

        this.item.update(dataToUpdate);
    }

    /*_onLevelChange(event) {
        if(this.item.type == "weapon") {
            console.log("onLevelChange");
            console.log(this.item);
            let datas = this.getData();
            let newLevel = event.target.value;
            let newBonusDamages = datas.config.weaponTypes[this.item.data.data.type].bonusDamages[newLevel];
            console.log(newBonusDamages);
            let dataToUpdate = {
                data: {
                    level: newLevel,
                    bonusDamages: newLevel
                }
            };

            this.item.update(dataToUpdate);
        }
    }*/
}