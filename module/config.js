export const fd = {};

let resourcesDir = "systems/fkng_doors/resources/";
let iconsDir = resourcesDir + "icons/";

fd.icons = {
    skill: {
        combat: iconsDir + "skill_combat.png",
        standard: iconsDir + "skill_standard.png",
        passive: iconsDir + "skill_passive.png"
    },
    storage: {
        backpack: iconsDir + "storage_backpack.png",
        belt: iconsDir + "storage_belt.png",
        pouch: iconsDir + "storage_pouch.png",
        unstored: iconsDir + "storage_unstored.png",
    },
    stats: {
        strength: iconsDir + "stat_str.jpg",
        agility: iconsDir + "stat_agi.jpg",
        intelligence: iconsDir + "stat_int.jpg",
        charisma: iconsDir + "stat_cha.jpg",
        luck: iconsDir + "stat_luc.jpg",
        senses: iconsDir + "stat_sen.jpg",

    },
    misc: {
        meal: iconsDir + "misc_meal.png"
    }
}
fd.weaponTypes = {
    ranged_medium: {
        name: "fd.items.weapon.types.ranged_medium",
        formula: "@agility",
        bonusDamages: {
            "1": " + 0",
            "2": " + 1d2",
            "3": " + 1 + 1d2",
            "4": " + 2d2",
            "5": " + 2 + 2d2",
            "6": " + 3d2",
            "7": " + 3 + 3d2",
            "8": " + 4d2",
            "9": " + 4 + 4d2",
            "10": " + 5d2",
            "11": " + 5 + 5d2",
            "12": " + 6d2",
            "13": " + 6 + 6d2",
            "14": " + 7d2",
            "15": " + 7 + 7d2",
            "16": " + 8d2",
            "17": " + 8 + 8d2",
            "18": " + 9d2",
            "19": " + 9 + 9d2",
            "20": " + 10d2"
        }
    },
    ranged_large: {
        name: "fd.items.weapon.types.ranged_large",
        formula: "1/2 * @agility + 1/2 * @strength",
        bonusDamages: {
            "1": " + 0",
            "2": " + 1d2",
            "3": " + 1 + 1d2",
            "4": " + 2d2",
            "5": " + 2 + 2d2",
            "6": " + 3d2",
            "7": " + 3 + 3d2",
            "8": " + 4d2",
            "9": " + 4 + 4d2",
            "10": " + 5d2",
            "11": " + 5 + 5d2",
            "12": " + 6d2",
            "13": " + 6 + 6d2",
            "14": " + 7d2",
            "15": " + 7 + 7d2",
            "16": " + 8d2",
            "17": " + 8 + 8d2",
            "18": " + 9d2",
            "19": " + 9 + 9d2",
            "20": " + 10d2"
        }
    },
    heavy_2handed: {
        name: "fd.items.weapon.types.heavy_2handed",
        formula: "@strength",
        bonusDamages: {
            "1": " + 0",
            "2": " + 1d2",
            "3": " + 1 + 1d2",
            "4": " + 2d2",
            "5": " + 2 + 2d2",
            "6": " + 3d2",
            "7": " + 3 + 3d2",
            "8": " + 4d2",
            "9": " + 4 + 4d2",
            "10": " + 5d2",
            "11": " + 5 + 5d2",
            "12": " + 6d2",
            "13": " + 6 + 6d2",
            "14": " + 7d2",
            "15": " + 7 + 7d2",
            "16": " + 8d2",
            "17": " + 8 + 8d2",
            "18": " + 9d2",
            "19": " + 9 + 9d2",
            "20": " + 10d2"
        }
    },
    light_1handed: {
        name: "fd.items.weapon.types.light_1handed",
        formula: "1/2 * @agility + 1/2 * @strength",
        bonusDamages: {
            "1": " + 0",
            "2": " + 1d2",
            "3": " + 1 + 1d2",
            "4": " + 2d2",
            "5": " + 2 + 2d2",
            "6": " + 3d2",
            "7": " + 3 + 3d2",
            "8": " + 4d2",
            "9": " + 4 + 4d2",
            "10": " + 5d2",
            "11": " + 5 + 5d2",
            "12": " + 6d2",
            "13": " + 6 + 6d2",
            "14": " + 7d2",
            "15": " + 7 + 7d2",
            "16": " + 8d2",
            "17": " + 8 + 8d2",
            "18": " + 9d2",
            "19": " + 9 + 9d2",
            "20": " + 10d2"
        }
    },
    medium_1handed: {
        name: "fd.items.weapon.types.medium_1handed",
        formula: "@strength",
        bonusDamages: {
            "1": " + 0",
            "2": " + 1d2",
            "3": " + 1 + 1d2",
            "4": " + 2d2",
            "5": " + 2 + 2d2",
            "6": " + 3d2",
            "7": " + 3 + 3d2",
            "8": " + 4d2",
            "9": " + 4 + 4d2",
            "10": " + 5d2",
            "11": " + 5 + 5d2",
            "12": " + 6d2",
            "13": " + 6 + 6d2",
            "14": " + 7d2",
            "15": " + 7 + 7d2",
            "16": " + 8d2",
            "17": " + 8 + 8d2",
            "18": " + 9d2",
            "19": " + 9 + 9d2",
            "20": " + 10d2"
        }
    },
    short_1handed: {
        name: "fd.items.weapon.types.short_1handed",
        formula: "@agility",
        bonusDamages: {
            "1": " + 0",
            "2": " + 1d2",
            "3": " + 1 + 1d2",
            "4": " + 2d2",
            "5": " + 2 + 2d2",
            "6": " + 3d2",
            "7": " + 3 + 3d2",
            "8": " + 4d2",
            "9": " + 4 + 4d2",
            "10": " + 5d2",
            "11": " + 5 + 5d2",
            "12": " + 6d2",
            "13": " + 6 + 6d2",
            "14": " + 7d2",
            "15": " + 7 + 7d2",
            "16": " + 8d2",
            "17": " + 8 + 8d2",
            "18": " + 9d2",
            "19": " + 9 + 9d2",
            "20": " + 10d2"
        }
    },
    polearm: {
        name: "fd.items.weapon.types.polearm",
        formula: "3/4 * @agility + 1/4 * @strength",
        bonusDamages: {
            "1": " + 0",
            "2": " + 1d2",
            "3": " + 1 + 1d2",
            "4": " + 2d2",
            "5": " + 2 + 2d2",
            "6": " + 3d2",
            "7": " + 3 + 3d2",
            "8": " + 4d2",
            "9": " + 4 + 4d2",
            "10": " + 5d2",
            "11": " + 5 + 5d2",
            "12": " + 6d2",
            "13": " + 6 + 6d2",
            "14": " + 7d2",
            "15": " + 7 + 7d2",
            "16": " + 8d2",
            "17": " + 8 + 8d2",
            "18": " + 9d2",
            "19": " + 9 + 9d2",
            "20": " + 10d2"
        }
    },
    opportunity: {
        name: "fd.items.weapon.types.opportunity",
        formula: "",
        bonusDamages: {
            "1": " + 0",
            "2": " + 1d2",
            "3": " + 1 + 1d2",
            "4": " + 2d2",
            "5": " + 2 + 2d2",
            "6": " + 3d2",
            "7": " + 3 + 3d2",
            "8": " + 4d2",
            "9": " + 4 + 4d2",
            "10": " + 5d2",
            "11": " + 5 + 5d2",
            "12": " + 6d2",
            "13": " + 6 + 6d2",
            "14": " + 7d2",
            "15": " + 7 + 7d2",
            "16": " + 8d2",
            "17": " + 8 + 8d2",
            "18": " + 9d2",
            "19": " + 9 + 9d2",
            "20": " + 10d2"
        }
    }
}

fd.stats = {
    "strength": {
        "name": "fd.actors.commons.strength",
        "img": fd.icons.stats.strength
    },
    "charisma": {
        "name": "fd.actors.commons.charisma",
        "img": fd.icons.stats.charisma
    },
    "agility": {
        "name": "fd.actors.commons.agility",
        "img": fd.icons.stats.agility
    },
    "intelligence": {
        "name": "fd.actors.commons.intelligence",
        "img": fd.icons.stats.intelligence
    },
    "health": {
        "name": "fd.actors.commons.health",
        "img": "icons/magic/life/cross-flared-green.webp"
    },
    "mana": {
        "name": "fd.actors.commons.mana",
        "img": "icons/consumables/drinks/alcohol-spirits-bottle-blue.webp"
    },
    "luck": {
        "name": "fd.actors.commons.luck",
        "img": fd.icons.stats.luck
    },
    "senses": {
        "name": "fd.actors.common.senses",
        "img": fd.icons.stats.senses
    }
};

fd.armorSlots = {
    head: "fd.items.armor.slots.head",
    torso: "fd.items.armor.slots.torso",
    arm: "fd.items.armor.slots.arm",
    hand: "fd.items.armor.slots.hand",
    leg: "fd.items.armor.slots.leg",
    foot: "fd.items.armor.slots.foot",
    shield: "fd.items.armor.slots.shield"
};

fd.armorTypes = {
    cloth: {
        "name": "fd.items.armor.types.cloth",
        "reduction": "1"
    },
    leather: {
        "name": "fd.items.armor.types.leather",
        "reduction": "2"
    },
    mail: {
        "name": "fd.items.armor.types.mail",
        "reduction": "3"
    },
    plate: {
        "name": "fd.items.armor.types.plate",
        "reduction": "4"
    },
    shieldPlate: {
        "name": "fd.items.armor.shield.types.plate",
        "reduction": "3"
    },
    shieldHardenedWood: {
        "name": "fd.items.armor.shield.types.hardenedWood",
        "reduction": "2"
    },
    shieldWood: {
        "name": "fd.items.armor.shield.types.wood",
        "reduction": "1"
    }
}

fd.jobs = {
    mage: {
        key: "mage",
        name: "fd.actors.playableCharacter.jobs.mage",
        attributes: {
            intelligence: {
                native: 8
            },
            agility: {
                native: 4
            },
            mana: {
                native: 2
            }
        }
    },
    warrior: {
        key: "warrior",
        name: "fd.actors.playableCharacter.jobs.warrior",
        attributes: {
            strength: {
                native: 8
            },
            agility: {
                native: 4
            }
        }
    },
    ranger: {
        key: "ranger",
        name: "fd.actors.playableCharacter.jobs.ranger",
        attributes: {
            strength: {
                native: 3
            },
            agility: {
                native: 3
            },
            intelligence: {
                native: 3
            },
            charisma: {
                native: 3
            },
            luck: {
                native: 3
            }
        }
    },
    hunter: {
        key: "hunter",
        name: "fd.actors.playableCharacter.jobs.hunter",
        attributes: {
            agility: {
                native: 8
            },
            charisma: {
                native: 4
            },
            luck: {
                native: 2
            }
        },

    },
    cleric: {
        key: "cleric",
        name: "fd.actors.playableCharacter.jobs.cleric",
        attributes: {
            intelligence: {
                native: 5
            },
            charisma: {
                native: 7
            },
            mana: {
                native: 2
            },
            luck: {
                native: 6
            }
        }
    },
    paladin: {
        key: "paladin",
        name: "fd.actors.playableCharacter.jobs.paladin",
        attributes: {
            strength: {
                native: 7
            },
            charisma: {
                native: 5
            },
            luck: {
                native: 2
            }
        }
    }
}

fd.races = {
    human: {
        key: "human",
        name: "fd.actors.playableCharacter.races.human",
        attributes: {
            strength: {
                native: 4
            },
            agility: {
                native: 4
            },
            intelligence: {
                native: 4
            },
            charisma: {
                native: 4
            },
            luck: {
                native: 5
            }
        }
    },
    elf: {
        key: "elf",
        name: "fd.actors.playableCharacter.races.elf",
        attributes: {
            intelligence: {
                native: 4
            },
            charisma: {
                native: 5
            },
            agility: {
                native: 5
            },
            health: {
                native: 1
            },
            mana: {
                native: 2
            }
        }
    },
    dwarf: {
        key: "dwarf",
        name: "fd.actors.playableCharacter.races.dwarf",
        attributes: {
            strength: {
                native: 6
            },
            agility: {
                native: 3
            },
            intelligence: {
                native: 4
            },
            charisma: {
                native: 3
            },
            luck: {
                native: 8
            },
            health: {
                native: 4
            }
        }
    },
    fae: {
        key: "fae",
        name: "fd.actors.playableCharacter.races.fae",
        attributes: {
            intelligence: {
                native: 5
            },
            strength: {
                native: 4
            },
            agility: {
                native: 5
            },
            health: {
                native: 1
            },
            mana: {
                native: 1
            },
            luck: {
                native: 12
            }
        }
    },
    barbarian: {
        key: "barbarian",
        name: "fd.actors.playableCharacter.races.barbarian",
        attributes: {
            strength: {
                native: 5
            },
            agility: {
                native: 5
            },
            charisma: {
                native: 4
            },
            health: {
                native: 5
            },
            luck: {
                native: 4
            }
        }
    },
    animorph: {
        key: "animorph",
        name: "fd.actors.playableCharacter.races.animorph",
        attributes: {
            intelligence: {
                native: 5
            },
            strength: {
                native: 3
            },
            charisma: {
                native: 6
            },
            mana: {
                native: 4
            },
            luck: {
                native: 10
            }
        }
    }
}

fd.experienceThresholds = {
    1: 300,
    2: 900,
    3: 2700,
    4: 6500,
    5: 14000,
    6: 23000,
    7: 34000,
    8: 48000,
    9: 64000,
    10: 85000,
    11: 100000,
    12: 120000,
    13: 140000,
    14: 165000,
    15: 195000,
    16: 225000,
    17: 265000,
    18: 305000,
    19: 355000,
    20: "."
}

fd.potionTypes = {
    "healthPotion": "fd.items.potion.types.healthPotion",
    "healthPowder": "fd.items.potion.types.healthPowder",
    "statPotion": "fd.items.potion.types.statPotion",
}

fd.storageContainers = {
    backpack: "fd.items.bag.backpack",
    belt: "fd.items.bag.belt",
    pouch: "fd.items.bag.pouch",
    unstored: "fd.items.bag.unstored"
}

fd.itemSortingFunction = function (a, b) {
    if (a instanceof Object && a.hasOwnProperty("name") &&
        b instanceof Object && b.hasOwnProperty("name")) {
        if (a.name < b.name) return -1;
        else if (a.name > b.name) return 1;
        else return 0;
    }
}