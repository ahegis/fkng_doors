var chatTemplate = {
    "stat": "systems/fkng_doors/templates/partials/roll/stat-roll.hbs",
    "potion": "systems/fkng_doors/templates/partials/roll/potion-roll.hbs",
    "item": "systems/fkng_doors/templates/partials/roll/item-roll.hbs",
    "aptitude": "systems/fkng_doors/templates/partials/roll/aptitude-roll.hbs",
    "armor": "systems/fkng_doors/templates/partials/roll/armor-roll.hbs",
    "weapon-basic": "systems/fkng_doors/templates/partials/roll/weapon-basic-roll.hbs"
}

var dialogTemplate = {
    "weapon": "systems/fkng_doors/templates/chat/weapon-dialog.hbs",
    "armor": "systems/fkng_doors/templates/chat/armor-damage-reduction-dialog.hbs",
    "item-no-count": "systems/fkng_doors/templates/chat/item-no-count-dialog.hbs",
    "spell-no-mana": "systems/fkng_doors/templates/chat/spell-no-mana-dialog.hbs",
    "armor-no-durability": "systems/fkng_doors/templates/chat/armor-no-durability-dialog.hbs"
}

var baseMessageData = {
    type: CONST.CHAT_MESSAGE_TYPES.ROLL,
}

export async function rollStat(statName, statImg, stat, actor) {
    let rollFormula = statName == game.i18n.localize("fd.actors.commons.luck") ? "1d100" : "1d20";

    let rollObject = new Roll(rollFormula, {}).roll({
        async: true
    });

    let messageData = {
        ...baseMessageData,
        speaker: ChatMessage.getSpeaker(),
        roll: rollObject
    }

    let rollResult = Math.ceil(rollObject.total);
    let statValue = stat.modifier ? stat.value + stat.modifier : stat.value;

    let cardData = {
        roll_result: rollResult,
        stat_img: statImg,
        stat_name: statName,
        stat_value: statValue,
        success: (rollResult <= statValue),
        owner: actor.id
    };

    messageData.content = await renderTemplate(chatTemplate["stat"], cardData);
    return ChatMessage.create(messageData);
}

export async function rollAptitude(aptitude, actor, gmOnly) {
    let rolls = [];
    let manaCost = aptitude.data.data.hasOwnProperty("manaCost") ? aptitude.data.data.manaCost : null;
    if (actor.data.data.mana.value < manaCost) {
        // If not enough mana for the spell
        const html = await renderTemplate(dialogTemplate["spell-no-mana"], {
            spell_data: aptitude
        });
        return new Promise(resolve => {
            const data = {
                title: game.i18n.localize("fd.chat.dialog.aptitude.title"),
                content: html,
                buttons: {
                    close: {
                        label: game.i18n.localize("fd.chat.dialog.commons.close"),
                        callback: html => resolve({
                            cancelled: true
                        })
                    }
                },
                default: "close",
                close: () => resolve({
                    cancelled: true
                })
            }
            new Dialog(data, null).render(true);
        });
    }

    let updateActor = {
        data: {
            mana: {
                value: actor.data.data.mana.value - manaCost
            }
        }
    }
    actor.update(updateActor)

    let messageData = {
        speaker: ChatMessage.getSpeaker()
    }

    if (aptitude.data.data.isPassive) {
        let cardData = {
            is_passive: true,
            p_aptitude: aptitude,
            aptitude_rpDescription: aptitude.data.data.rpDescription,
            owner: actor.id
        }

        messageData.content = await renderTemplate(chatTemplate["aptitude"], cardData);
    } else {
        messageData = {
            ...messageData,
            ...baseMessageData
        }

        let statsObject = _geActorStatsObject(actor);

        let mainFormula = aptitude.data.data.mainFormula;
        let p_bonus = aptitude.data.data.bonus;
        let mainRoll = await new Roll(mainFormula, statsObject).evaluate({
            async: true
        });
        mainRoll.dice[0].options.rollOrder = 1;
        rolls.push(mainRoll);

        let targetFormula = aptitude.data.data.rollingAgainst;
        let targetRoll = !aptitude.data.data.isSimpleRoll ? new Roll(targetFormula + " + " + p_bonus, statsObject).roll({
            async: true
        }) : {
            total: 0
        };

        let additionalFormula = aptitude.data.data.additionalRoll;
        let additionalRoll = aptitude.data.data.hasAdditionalRoll ? await new Roll(additionalFormula, statsObject).evaluate({
            async: true
        }) : {
            total: 0
        };
        if (additionalRoll.dice != undefined && additionalRoll.dice[0] != undefined) {
            additionalRoll.dice[0].options.rollOrder = 2;
            rolls.push(additionalRoll);
        }

        if (rolls != []) {
            let mergingPool = new DicePool({
                rolls: rolls
            });
            let messageRoll = Roll.create(mergingPool.formula);
            messageRoll.terms = [mergingPool]
            messageRoll.results = [mergingPool.total];
            messageRoll._total = mergingPool.total;
            messageRoll._rolled = true;

            messageData.roll = messageRoll;
        }

        let cardData = {
            is_passive: false,
            p_aptitude: aptitude,
            aptitude_rpDescription: aptitude.data.data.rpDescription,
            main_roll_result: Math.ceil(mainRoll.total),
            is_simple_roll: aptitude.data.data.isSimpleRoll,
            main_roll_target_formula: targetRoll.result,
            main_roll_target: targetRoll.total,
            bonus: p_bonus,
            has_additional_roll: aptitude.data.data.hasAdditionalRoll,
            additional_roll_result: additionalRoll.total,
            gm_only: gmOnly,
            success: (Math.ceil(mainRoll.total) <= targetRoll.total),
            owner: actor.id
        }

        messageData.content = await renderTemplate(chatTemplate["aptitude"], cardData);
        if (gmOnly) messageData = ChatMessage.applyRollMode(messageData, CONST.DICE_ROLL_MODES.BLIND)
    }

    let message = ChatMessage.create(messageData);
    return message
}

export async function getArmorDialog(armor, armorType) {
    let messageData = {
        speaker: ChatMessage.getSpeaker(),
    };

    let cardData = {
        p_armor: armor,
        armor_damage_reduction: armor.data.data.durability > 0 ? armorType.reduction : 0,
        durability_message: armor.data.data.durability > 0 ? armor.data.data.durability : "0 (" + game.i18n.localize("fd.rolls.armor.broken") + ")",
        armor_rpDescription: armor.data.data.rpDescription,
    }

    messageData.content = await renderTemplate(chatTemplate["armor"], cardData);
    return ChatMessage.create(messageData);
}


export async function rollItem(item) {
    //Check that there are still count of object available
    if (item.data.data.count && item.data.data.count > 0) {
        if (item.data.type == "potion") {
            let updateCount = {
                data: {
                    count: item.data.data.count - 1
                }
            }
            let rollFormula = item.data.data.formula;
            let rollObject = new Roll(rollFormula, {}).roll({
                async: true
            });
            let potionEffectString;
            switch (item.data.data.type) {
                case "healthPotion":
                case "healthPowder":
                    potionEffectString = "fd.rolls.potion.effects.heal";
                    break;
                case "statPotion":
                    potionEffectString = "fd.rolls.potion.effects.statBonus";
                    break;
                default:
                    potionEffectString = "fd.rolls.potion.effects.default";
                    break;
            }
            let messageData = {
                ...baseMessageData,
                speaker: ChatMessage.getSpeaker(),
                roll: rollObject
            }
            let cardData = {
                p_item: item,
                potion_name: item.name,
                potion_effect: game.i18n.localize(potionEffectString),
                roll_result: Math.ceil(rollObject.total)
            }
            messageData.content = await renderTemplate(chatTemplate["potion"], cardData);

            item.update(updateCount);
            return ChatMessage.create(messageData);
        } else {
            let messageData = {
                speaker: ChatMessage.getSpeaker()
            }

            let cardData = {
                p_item: item,
                item_rpDescription: item.data.data.rpDescription,
            }
            messageData.content = await renderTemplate(chatTemplate["item"], cardData);
            return ChatMessage.create(messageData);
        }
    } else {
        // If no more count of the object
        const html = await renderTemplate(dialogTemplate["item-no-count"], {
            item_data: item
        });
        return new Promise(resolve => {
            const data = {
                title: game.i18n.localize("fd.chat.dialog.weapon.title"),
                content: html,
                buttons: {
                    close: {
                        label: game.i18n.localize("fd.chat.dialog.commons.close"),
                        callback: html => resolve({
                            cancelled: true
                        })
                    }
                },
                default: "close",
                close: () => resolve({
                    cancelled: true
                })
            }
            new Dialog(data, null).render(true);
        });
    }
}

export async function rollWeaponBasicAttack(weapon, actor) {
    let rolls = [];
    let weaponStatFormula = weapon.data.data.statsFormula;
    let statRollData = _geActorStatsObject(actor);
    let statRollObject = new Roll(weaponStatFormula, statRollData).roll({
        async: true
    });
    let statTarget = Math.ceil(statRollObject.total);

    let rollObject = new Roll("1d20", {}).evaluate({
        async: true
    });
    rollObject.dice[0].options.rollOrder = 1;
    rolls.push(rollObject);
    let rollResult = Math.ceil(rollObject.total);

    let bonusDamagesValue = 0;
    if (weapon.data.data.bonusDamages != "") {
        let bonusDamageObject = new Roll(weapon.data.data.bonusDamages, statRollData).evaluate({
            async: true
        });
        if (bonusDamageObject.dice != undefined && bonusDamageObject.dice[0] != undefined)
            bonusDamageObject.dice[0].options.rollOrder = 2;
        rolls.push(bonusDamageObject);
        bonusDamagesValue = Math.ceil(bonusDamageObject.total);
    }
    let attackDamages = statTarget - (rollResult - 1);
    let damageValue = Math.max(attackDamages + bonusDamagesValue, 0);


    let mergingPool = new DicePool({
        rolls: rolls
    });
    let messageRoll = Roll.create(mergingPool.formula);
    messageRoll.terms = [mergingPool]
    messageRoll.results = [mergingPool.total];
    messageRoll._total = mergingPool.total;
    messageRoll._rolled = true;

    let messageData = {
        ...baseMessageData,
        speaker: ChatMessage.getSpeaker(),
        roll: messageRoll
    };

    let cardData = {
        p_weapon: weapon,
        roll_result: rollResult,
        weapon_name: weapon.name,
        weapon_formula: weaponStatFormula,
        weapon_target_value: statTarget,
        attack_damages: attackDamages,
        bonus_damages: bonusDamagesValue,
        damage_value: damageValue,
        success: (rollResult <= statTarget),
        owner: actor.id
    };
    messageData.content = await renderTemplate(chatTemplate["weapon-basic"], cardData);
    return ChatMessage.create(messageData);
}

function _geActorStatsObject(actor) {
    console.log(actor);
    let actorData = actor.data.data
    let strengthStat = actorData.strength;
    let agilityStat = actorData.agility;
    let charismaStat = actorData.charisma;
    let intelligenceStat = actorData.intelligence;
    let luckStat = actorData.luck;
    let sensesStat = actorData.senses

    let statsObject = {
        strength: strengthStat != undefined ? strengthStat.value : 0,
        force: strengthStat != undefined ? strengthStat.value : 0,
        agility: agilityStat != undefined ? agilityStat.value : 0,
        agilite: agilityStat != undefined ? agilityStat.value : 0,
        charisma: charismaStat != undefined ? charismaStat.value : 0,
        charisme: charismaStat != undefined ? charismaStat.value : 0,
        intelligence: intelligenceStat != undefined ? intelligenceStat.value : 0,
        senses: sensesStat != undefined ? sensesStat.value : 0,
        sens: sensesStat != undefined ? sensesStat.value : 0
    }

    if (actor.data.type === "pc") {
        statsObject.strength += strengthStat.modifier;
        statsObject.force += strengthStat.modifier;
        statsObject.agility += agilityStat.modifier;
        statsObject.agilite += agilityStat.modifier;
        statsObject.charisma += charismaStat.modifier;
        statsObject.charisme += charismaStat.modifier;
        statsObject.intelligence += intelligenceStat.modifier;

        statsObject.luck = luckStat != undefined ? luckStat.value : 0;
        statsObject.chance = luckStat != undefined ? luckStat.value : 0;
    }

    return statsObject;
}